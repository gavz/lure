# SERVER CONFIG
GOPHISH_API_KEY = "<<api key>>"
HUNTERIO_API_KEY = "<<api key>>"
BING_API_KEY = "<<api key>>"
BING_ENDPOINT = "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search?q="
GOPHISH_SERVER = "https://<<phishingserver>>"
GOPHISH_PORT = 3333
BASE_URL = GOPHISH_SERVER + ":" + str(GOPHISH_PORT)
TESTER_NAME = "<<configure me>>"
VERSION = "Lure v0.1"

# DISCOVERY CONFIG
HUNTERIO = True
THEHARVESTER = False
LINKEDIN = True

TIMEOUT = 5
HARVESTER_LOCATION = "/usr/share/theHarvester/theHarvester.py"
